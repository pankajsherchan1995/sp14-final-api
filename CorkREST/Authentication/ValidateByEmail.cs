﻿using CorkREST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Http.Controllers;


namespace CorkREST.Authentication
{
    public class ValidateByEmail : AuthorizeAttribute
    {
        private Entities1 db = new Entities1();
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext.Request.Headers.Contains(HeaderValues.Email) &&
                actionContext.Request.Headers.Contains(HeaderValues.Password))
            {

                if (actionContext.Request.Headers.GetValues(HeaderValues.Email).FirstOrDefault() != null)
                {
                    // get value from header
                    var userEmail = Convert.ToString(actionContext.Request.Headers.GetValues(HeaderValues.Email).First());

                    var user = db.Users.FirstOrDefault(u => u.Email.Equals(userEmail));

                    if (user != null)
                    {

                        if (actionContext.Request.Headers.GetValues(HeaderValues.Password).FirstOrDefault() != null)
                        {
                            var password = actionContext.Request.Headers.GetValues(HeaderValues.Password).First();

                            if (!Crypto.VerifyHashedPassword(user.Password, password))
                            {
                                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "Credentials do Not Match");
                                return;
                            }

                            user.ApiKey = GetApiKey();
                            db.SaveChanges();

                            HttpContext.Current.Response.Headers.Add("AuthenticationStatus", "Authorized");
                            HttpContext.Current.Response.Headers.Add("ApiKey", user.ApiKey);
                            HttpContext.Current.Response.Headers.Add("UserId", user.Id.ToString());
                            return;
                        }
                    }

                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "Invalid username or password;");
                    return;
                }
            }

           actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.ExpectationFailed, "Key values " + HeaderValues.Email + " and/or "+ HeaderValues.Password + " were not included in your request.");
           return;
        }

        private static string GetApiKey()
        {
            using (var rng = new RNGCryptoServiceProvider())
            {
                var bytes = new byte[16];
                rng.GetBytes(bytes);
                return Convert.ToBase64String(bytes);
            }
        }

    }
}