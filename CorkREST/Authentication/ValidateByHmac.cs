﻿using CorkREST.Areas.API.Controllers;
using CorkREST.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.Results;

namespace CorkREST.Authentication
{
    public class ValidateByHmac : AuthorizeAttribute
    {
        private const string UnauthorizedMessage = "Unauthorized request";
        private Entities1 db;
        private readonly IBuildMessageRepresentation _representationBuilder = new CanonicalRepresentationBuilder();
        private readonly ICalculteSignature _signatureCalculator = new HmacSignatureCalculator();

        public override void OnAuthorization(HttpActionContext context)
        {
            var controller = (BaseApiController)context.ControllerContext.Controller;
            db = controller._db;

            var request = context.Request;

            if (!(request.Headers.Contains(HeaderValues.UserId) && request.Headers.Contains(HeaderValues.HmacDigest)))
            {
                context.Response = request.CreateResponse(HttpStatusCode.ExpectationFailed, "Key values 'x-cmps-383-authentication-id' and/or 'x-cmps-383-authentication-key' were not included in your request.");
                return;
            }

            var userIdString = Convert.ToString(request.Headers.GetValues(HeaderValues.UserId).FirstOrDefault());
            int userId;

            if (!(int.TryParse(userIdString, out userId)))
            {
                context.Response = request.CreateResponse(HttpStatusCode.Unauthorized, "Key value 'x-cmps-383-authentication-id' is not valid");
                return;
            }

            var user = db.Users.FirstOrDefault(u => u.Id == userId);

            if (user == null)
            {
                context.Response = request.CreateResponse(HttpStatusCode.Unauthorized, "Key value 'x-cmps-383-authentication-id' is not valid");
                return;
            }

            if (request.Headers.GetValues(HeaderValues.HmacDigest) == null)
            {
                context.Response = request.CreateResponse(HttpStatusCode.Unauthorized, "Key value 'x-cmps-383-authentication-digest' is not valid");
                return;
            }

            var digest = request.Headers.GetValues(HeaderValues.HmacDigest).First();

            var representation = _representationBuilder.BuildRequestRepresentation(request);
            if (representation == null)
            {
                context.Response = request.CreateResponse(HttpStatusCode.Unauthorized, "Key value 'x-cmps-383-authentication-digest' is not valid");
                return;
            }

            var signature =  _signatureCalculator.Signature(user.ApiKey, representation);

            if (!digest.Equals( signature))
            {
                context.Response = request.CreateResponse(HttpStatusCode.Unauthorized, "Key value 'x-cmps-383-authentication-digest' is not valid");
                return;
            }
            controller.CorkUser = user;
            HttpContext.Current.Response.AddHeader("AuthenticationStatus", "Authorized");
        }
    }
}
