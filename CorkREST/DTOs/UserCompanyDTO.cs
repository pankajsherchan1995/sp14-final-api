﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorkREST.DTOs
{
    public class UserCompanyDTO
    {
        public int UserId { get; set; }
        public int CompanyId { get; set; }
    }
}