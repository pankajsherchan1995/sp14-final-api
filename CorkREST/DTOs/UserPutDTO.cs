﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorkREST.DTOs
{
    public class UserPutDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int? RemoveFromCompanyId { get; set; }
        public int? RemoveFromRoleId { get; set; }

        public int? AddToCompanyId { get; set; }
        public int? AddToRoleId { get; set; }
    }
}