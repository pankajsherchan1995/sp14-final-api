﻿
namespace CorkREST.Models
{
    public class ApiKeyDTO
    {
        public string ApiKey { get; set; }
        public string UserId { get; set; }
    }
}