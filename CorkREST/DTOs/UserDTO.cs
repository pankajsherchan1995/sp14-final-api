﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorkREST.DTOs
{
    public class UserDTO
    {
        public int Id { get; set; }
       
        
        public int AssignToRoleId { get; set; }
        public int AssignToCompanyId { get; set; }
        
       
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}