﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorkREST.DTOs
{
    public class RoleDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}