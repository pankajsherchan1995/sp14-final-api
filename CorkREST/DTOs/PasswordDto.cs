﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorkREST.DTOs
{
    public class PasswordDto
    {
        public string Password { get; set; }
        public string Confirm { get; set; }
    }
}