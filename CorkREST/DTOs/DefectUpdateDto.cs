﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorkREST.DTOs
{
    public class DefectUpdateDto
    {

            public int Id { get; set; }

            public string Notes { get; set; }

            public int WorkflowStepId { get; set; }
        

    }
}