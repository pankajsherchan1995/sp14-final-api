﻿using CorkREST.Models;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace CorkREST.Areas.API.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        public User CorkUser;
        public Entities1 _db;

        protected BaseApiController(Entities1 db) 
        {
            CorkUser = new User();
            _db = db;
        }

        protected bool IsCompanyAdmin()
        {
            if (IsSystemAdmin())
            {
                return true;
            }

            return CorkUser.Roles.Select(r => r.Id).Contains(2);
        }

        protected bool IsSystemAdmin()
        {
            return CorkUser.Roles.Select(r => r.Id).Contains(1);
        }

        protected bool CanAccessCompany(int companyId)
        {
            if (IsSystemAdmin())
            {
                return true;
            }

            return CorkUser.Companies.Select(c => c.Id).Contains(companyId);
        }

        protected bool CanAccessProject(int projectID)
        {
            if (IsSystemAdmin())
            {
                return true;
            }

            return CorkUser.Companies.SelectMany(c => c.Projects).Select(p => p.Id).Contains(projectID);
        }

        protected bool CanAccessDefect(int defectID)
        {
            if (IsSystemAdmin())
            {
                return true;
            }

            return CorkUser.Companies.SelectMany(c => c.Projects).SelectMany(p => p.Defects).Select(d => d.Id).Contains(defectID);
        }

        protected bool CanAccessUser(int userID)
        {
            if (IsSystemAdmin())
            {
                return true;
            }

            return CorkUser.Companies.SelectMany(c => c.Users).Select(u => u.Id).Contains(userID);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {

                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
