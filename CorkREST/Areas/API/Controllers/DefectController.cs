﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using CorkREST.Authentication;
using CorkREST.Models;
using CorkREST.DTOs;
using System.Linq.Expressions;
using System;
using System.Threading.Tasks;
using CorkREST.Enums;
using System.Collections.Generic;

namespace CorkREST.Areas.API.Controllers
{
    [ValidateByApiKey]
    [RoutePrefix("api/defect")]
    public class DefectController : BaseApiController
    {
        private DtoFactory dfac = new DtoFactory();

            public DefectController(Entities1 context) : base(context)
    {
        
    }
        // GET api/Defect/5
        /// <summary>
        /// Returns all Defects for defects in your company.
        /// </summary>
        [ResponseType(typeof(DefectDTO))]
        public IQueryable<DefectDTO> GetDefects()
        {
            if (!IsSystemAdmin())
            {
                return CorkUser.Companies.SelectMany(c => c.Projects).SelectMany(p => p.Defects).AsQueryable<Defect>().Select(dfac.AsDefectDTO);
            }
            
            var defects = _db.Companies.SelectMany(c => c.Projects).SelectMany(p => p.Defects).AsQueryable<Defect>().Select(dfac.AsDefectDTO);

            return defects;
        }

        /// <summary>
        /// Returns all defects for the listed company 
        /// </summary>
        /// <param name="companyId">The ID of the company to be queired against.</param>
        [Route("company/{companyId:int}")]
        public dynamic GetCompanyDefects(int companyId)
        {
            if (!CanAccessCompany(companyId))
            {
                return Unauthorized();
            }

            if (!IsSystemAdmin())
            {
                return CorkUser.Companies.SelectMany(c => c.Projects).SelectMany(p => p.Defects).AsQueryable<Defect>().Select(dfac.AsDefectDTO);
            }
            
            return _db.Companies.SelectMany(c => c.Projects).SelectMany(p => p.Defects).AsQueryable<Defect>().Select(dfac.AsDefectDTO);
        }

        /// <summary>
        /// Returns all defects for the specified project 
        /// </summary>
        /// <param name="projectId">The ID of the project to be queired against.</param>
        [Route("project/{projectId:int}/defects")]
        public dynamic GetProjectDefects(int projectId)
        {
            if (!CanAccessProject(projectId))
            {
                return Unauthorized();
            }

            var project = _db.Companies.SelectMany(c => c.Projects).FirstOrDefault(p => p.Id == projectId);

            if (project == null)
            {
                return BadRequest();
            }

            var defects = project.Defects.AsQueryable<Defect>().Select(dfac.AsDefectDTO);

            return defects;
        }

        // GET api/Defect/5
        /// <summary>
        /// Returns the specified Defect
        /// </summary>
        /// <param name="id">The ID of the defect to be returned.</param>
        [ResponseType(typeof(DefectDTO))]
        public IHttpActionResult GetDefect(int id)
        {
            if (!CanAccessDefect(id))
            {
                return Unauthorized();
            }

            Defect defect = _db.Companies.SelectMany(c => c.Projects).SelectMany(p => p.Defects).Where(d =>  d.Id == id).FirstOrDefault();
 
            if (defect == null)
            {
                return NotFound();
            }

            return Ok(dfac.ToDefectDTO(defect));
        }

        /// <summary>
        /// Returns a list of defects where the DateFound is equal to the search parameter
        /// </summary>
        /// <param name="date">The DateFound of the defects to be returned. Accepts 2014-1-31 or 2014/1/31</param>
        [Route("foundon/{date:datetime}")]
        [Route("foundon/{*datef:datetime:regex(\\d{4}/\\d{2}/\\d{2})}")]
        public IQueryable<DefectDTO> GetDefectsByDateFound(DateTime date)
        {
            return CorkUser.Companies.SelectMany(c => c.Projects).SelectMany(p => p.Defects).Where(d => d.DateFound.Date
                    == date.Date).AsQueryable<Defect>().Select(dfac.AsDefectDTO);
        }

        /// <summary>
        /// Returns a list of defects where the DateFound is before to the search parameter
        /// </summary>
        /// <param name="date">The DateFound of the defects to be returned. Accepts 2014-1-31 or 2014/1/31</param>
        [Route("foundbefore/{date:datetime:regex(\\d{4}-\\d{2}-\\d{2})}")]
        [Route("foundbefore/{*date:datetime:regex(\\d{4}/\\d{2}/\\d{2})}")]
        public IQueryable<DefectDTO> GetDefectsFoundBefore(DateTime date)
        {
            return CorkUser.Companies.SelectMany(c => c.Projects).SelectMany(p => p.Defects).Where(d => d.DateFound.Date
                    <= date.Date).AsQueryable<Defect>().Select(dfac.AsDefectDTO);
        }

        /// <summary>
        /// Returns a list of defects where the DateFound is after to the search parameter
        /// </summary>
        /// <param name="date">The DateFound of the defects to be returned. Accepts 2014-1-31 or 2014/1/31</param>
        [Route("foundafter/{date:datetime:regex(\\d{4}-\\d{2}-\\d{2})}")]
        [Route("foundafter/{*date:datetime:regex(\\d{4}/\\d{2}/\\d{2})}")]
        public IQueryable<DefectDTO> GetDefectsFoundAfter(DateTime date)
        {
            return CorkUser.Companies.SelectMany(c => c.Projects).SelectMany(p => p.Defects).Where(d => d.DateFound
                    >= date).AsQueryable<Defect>().Select(dfac.AsDefectDTO);
        }

        /// <summary>
        /// Returns a list of defects where the DateFixed is equal to the search parameter
        /// </summary>
        /// <param name="date">The DateFound of the defects to be returned. Accepts 2014-1-31 or 2014/1/31</param>
        [Route("datefixed/{date:datetime:regex(\\d{4}-\\d{2}-\\d{2})}")]
        [Route("datefixed/{*date:datetime:regex(\\d{4}/\\d{2}/\\d{2})}")]
        public IQueryable<DefectDTO> GetDefectsByDateFixed(DateTime date)
        {
            return CorkUser.Companies.SelectMany(c => c.Projects).SelectMany(p => p.Defects).Where(d => d.DateFixed.HasValue && d.DateFixed.Value.Date
                    == date.Date).AsQueryable<Defect>().Select(dfac.AsDefectDTO);
        }

        /// <summary>
        /// Returns a list of defects where the DateFixed is before to the search parameter.
        /// </summary>
        /// <param name="date">The DateFixed of the defects to be returned. Accepts 2014-1-31 or 2014/1/31</param>
        [Route("fixedbefore/{date:datetime:regex(\\d{4}-\\d{2}-\\d{2})}")]
        [Route("fixedbefore/{*date:datetime:regex(\\d{4}/\\d{2}/\\d{2})}")]
        public IQueryable<DefectDTO> GetDefectsFixedBefore(DateTime date)
        {
            return CorkUser.Companies.SelectMany(c => c.Projects).SelectMany(p => p.Defects).Where(d => d.DateFixed.HasValue && d.DateFixed.Value.Date
                    <= date.Date).AsQueryable<Defect>().Select(dfac.AsDefectDTO);
        }

        /// <summary>
        /// Returns a list of defects where the DateFixed is after to the search parameter.
        /// </summary>
        /// <param name="date">The DateFixed of the defects to be returned. Accepts 2014-1-31 or 2014/1/31</param>
        [Route("fixedafter/{date:datetime:regex(\\d{4}-\\d{2}-\\d{2})}")]
        [Route("fixedafter/{*date:datetime:regex(\\d{4}/\\d{2}/\\d{2})}")]
        public IQueryable<DefectDTO> GetDefectsFixedAfter(DateTime date)
        {
            return CorkUser.Companies.SelectMany(c => c.Projects).SelectMany(p => p.Defects).Where(d => d.DateFixed.HasValue && d.DateFixed.Value.Date
                    >= date.Date).AsQueryable<Defect>().Select(dfac.AsDefectDTO);
        }

        // PUT api/Defect/5
        /// <summary>
        /// Returns an updated version of a specified defect. 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="defectDto"></param>
        /// <returns></returns>
        [Route("{id:int}/update")]
        //[AuthenticateByRole("CompanyAdmin,ProjectManager,QualityAssurance")]
        public IHttpActionResult PutDefectUpdate(int id, DefectUpdateDto defectDto)
        {
            if (!CanAccessDefect(id))
            {
                return Unauthorized();
            }

            if (id != defectDto.Id)
            {
                return BadRequest();
            }

            var defectFromDb = _db.Defects.FirstOrDefault(d => d.Id == id);

            if (defectFromDb == null)
            {
                return BadRequest();
            }

            List<int> allowedSteps = new List<int>{(int)WorkFlowStepEnum.InProgress, (int)WorkFlowStepEnum.ReadyforTesting, (int)WorkFlowStepEnum.NotReproducible };

            if (defectDto.WorkflowStepId > 0)
            {
                if (!(allowedSteps.Contains(defectDto.WorkflowStepId)))
                {
                    return Unauthorized();
                }

                var workFlowStep = _db.WorkflowSteps.Find(defectDto.WorkflowStepId);
                defectFromDb.WorkflowStep = workFlowStep;
            }

            if (defectDto.Notes != null) 
            {
                defectFromDb.Notes = defectDto.Notes;
            }

            _db.Entry(defectFromDb).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DefectExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok();
        }
        /// <summary>
        /// Returns the spcified updated version of the defect.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="defectDto"></param>
        /// <returns></returns>
        [AuthenticateByRole("CompanyAdmin,ProjectManager,QualityAssurance")]
        public IHttpActionResult PutDefect(int id, DefectDTO defectDto)
       {
            if (!CanAccessDefect(id)) 
            {
                return Unauthorized();
            }

            if (id != defectDto.Id)
            {
                return BadRequest();
            }

            var defect = dfac.ToDefect(defectDto);

            _db.Entry(defect).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DefectExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok();
        }
        /// <summary>
        /// Creates a new defect.
        /// </summary>
        /// <param name="defect"></param>
        /// <returns></returns>
        [AuthenticateByRole("CompanyAdmin,ProjectManager,QualityAssurance")]
        // POST api/Defect
        [ResponseType(typeof(DefectDTO))]
        public IHttpActionResult PostDefect(DefectDTO defect)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Defect newDefect = dfac.ToDefect(defect);

            _db.Defects.Add(newDefect);
            _db.SaveChanges();
            defect.Id=newDefect.Id;
            return CreatedAtRoute("DefaultApi", new { id = defect.Id },defect);
        }

        // DELETE api/Defect/5
        /// <summary>
        /// Deletes a specified defect.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AuthenticateByRole("CompanyAdmin,ProjectManager,QualityAssurance")]
        [ResponseType(typeof(DefectDTO))]
        public IHttpActionResult DeleteDefect(int id)
        {
            Defect defect = _db.Defects.Find(id);
            if (defect == null)
            {
                return NotFound();
            }

            _db.Defects.Remove(defect);
            _db.SaveChanges();

            return Ok(dfac.ToDefectDTO(defect));
        }

        private bool DefectExists(int id)
        {
            return _db.Defects.Count(e => e.Id == id) > 0;
        }


    }
}