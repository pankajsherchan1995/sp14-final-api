﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using CorkREST.Authentication;
using CorkREST.Models;
using CorkREST.DTOs;

namespace CorkREST.Areas.API.Controllers
{
    [ValidateByApiKey]
    public class CompanyController : BaseApiController
    {
        private DtoFactory dfac = new DtoFactory();

        public CompanyController(Entities1 context) : base(context)
        {
           // this._db = context;
        }
        
        // GET api/Company
        /// <summary>
        /// Returns a list of Companies
        /// </summary>
        /// <returns></returns>
        public IQueryable<CompanyDTO> GetCompanies()
        {
            if (!IsSystemAdmin()) 
            {
                return CorkUser.Companies.AsQueryable<Company>().Select(dfac.AsCompanyDTO);
            }
            return _db.Companies.AsQueryable<Company>().Select(dfac.AsCompanyDTO);
        }

        // GET api/Company/5
        /// <summary>
        /// Returns a specified Company.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(CompanyDTO))]
        public IHttpActionResult GetCompany(int id)
        {
            CompanyDTO company = _db.Companies
                .Where(c => c.Id == id)
                .Select(dfac.AsCompanyDTO)
                .FirstOrDefault();
            if (company == null)
            {
                return NotFound();
            }

            return Ok(company);
        }

        // POST api/Company/5
        /// <summary>
        /// Creates a new Company.
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        [AuthenticateByRole("SystemAdmin")]
        public IHttpActionResult PostCompany(CompanyDTO company)
        {
            var compFromDB = _db.Companies.FirstOrDefault(co => co.Id == company.Id);

            if (compFromDB == null)
            {
                return NotFound();
            }

            compFromDB.Name = company.Name;
            _db.Entry(compFromDB).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanyExists(company.Id))
                {
                    return NotFound();
                }
                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // Put api/Company
        /// <summary>
        /// Updates a Company.
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        [AuthenticateByRole("SystemAdmin")]
        [ResponseType(typeof(CompanyDTO))]
        public IHttpActionResult Put(CompanyDTO company)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Company addCompany = dfac.ToCompany(company);
            _db.Companies.Add(addCompany);
            _db.SaveChanges();
            company.Id = addCompany.Id;
            return CreatedAtRoute("DefaultApi", new { id = company.Id }, company);
        }

        // DELETE api/Company/5
        /// <summary>
        /// Deletes a specified Company.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AuthenticateByRole("SystemAdmin")]
        [ResponseType(typeof(Company))]
        public IHttpActionResult DeleteCompany(int id)
        {
            Company company = _db.Companies.Find(id);
            if (company == null)
            {
                return NotFound();
            }

            _db.Companies.Remove(company);
            _db.SaveChanges();

            return Ok(company);
        }

        private bool CompanyExists(int id)
        {
            return _db.Companies.Count(e => e.Id == id) > 0;
        }
    }
}