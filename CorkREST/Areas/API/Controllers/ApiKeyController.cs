﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using CorkREST.Authentication;
using CorkREST.Models;

namespace CorkREST.Areas.API.Controllers
{
    [ValidateByEmail]
    public class ApiKeyController : ApiController
    {
        // GET api/<controller>
        /// <summary>
        /// Returns API Keys
        /// </summary>
        /// <returns></returns>
        public ApiKeyDTO Get()
        {
            var apiKey = new ApiKeyDTO();
           apiKey.ApiKey =  HttpContext.Current.Response.Headers.GetValues("ApiKey").First();

           return new ApiKeyDTO { 
                                    ApiKey = HttpContext.Current.Response.Headers.GetValues("ApiKey").First(),
                                    UserId = HttpContext.Current.Response.Headers.GetValues("UserId").First()
                                  };
        }

        // GET api/<controller>/5
        /// <summary>
        /// Returns API Key of a specific user.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string Get(int id)
        {
            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "This method is not currently supported."));
        }

        // POST api/<controller>
        /// <summary>
        /// Creates an API Key.
        /// </summary>
        /// <param name="value"></param>
        public void Post([FromBody]string value)
        {
            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "This method is not currently supported."));

        }

        // PUT api/<controller>/5
        /// <summary>
        /// Updates a specified API Key.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        public void Put(int id, [FromBody]string value)
        {
            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "This method is not currently supported."));

        }

        // DELETE api/<controller>/5
        /// <summary>
        /// Deletes a specified API Key.
        /// </summary>
        /// <param name="id"></param>
        public void Delete(int id)
        {
            throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, "This method is not currently supported."));
        }
    }
}