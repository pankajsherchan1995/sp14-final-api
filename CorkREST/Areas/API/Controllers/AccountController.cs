﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using CorkREST.Authentication;
using CorkREST.Models;
using CorkREST.DTOs;
using System.Web.Helpers;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace CorkREST.Areas.API.Controllers
{
    [ValidateByApiKey]
    [RoutePrefix("Account")]
    public class AccountController : BaseApiController
    {
        private DtoFactory dfac = new DtoFactory();
            public AccountController(Entities1 context) : base(context)
    {
        
    }

        private static readonly Expression<Func<User, ReturnUserDTO>> AsReturnUserDTO =
            x => new ReturnUserDTO
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email
            };

        // GET api/Account
        /// <summary>
        /// Returns your account information.
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(ReturnUserDTO))]
        public IHttpActionResult Get()
        {
            return Ok(dfac.ToReturnUserDTO(CorkUser));
        }

        // POST api/Account/ChangePassword
        /// <summary>
        /// Changes password of a user.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!Crypto.VerifyHashedPassword(CorkUser.Password, model.OldPassword))
            {
                return Unauthorized();
            }

            CorkUser.Password = Crypto.HashPassword(model.NewPassword);

            _db.Entry(CorkUser).State = EntityState.Modified;

            try
            {
                _db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(CorkUser.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok();
        }

        private bool UserExists(int id)
        {
            return _db.Users.Count(e => e.Id == id) > 0;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

    }
}
